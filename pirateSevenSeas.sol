import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

// SPDX-License-Identifier: none

pragma solidity ^0.8.0;

contract pirateSevenSeas is Ownable, ReentrancyGuard {
    using Counters for Counters.Counter;

    uint256 immutable public maxX = 1000;
    uint256 immutable public maxY = 1000;
    uint256 immutable public maxDeep = 100;

    Counters.Counter private totalMapStructure;

    mapping (rewardRarity => mapping(rewardType => uint256)) private rewardRules;
    mapping (uint256 => mapping (uint256 => mapping(uint256 => rewardMap))) private mapStructure;
    mapping (uint256 => mapping (uint256 => mapping(uint256 => Counters.Counter))) private currentDeep;

    // for debug only, will removed
    struct rewardMapLocation{
        rewardType mysteryReward;
        rewardRarity mysteryRarity;
        uint256 rewardDeep;
        uint256 rewardX;
        uint256 rewardY;
    }
    struct listRewardLocation{
        rewardMapLocation[] commonKey;
        rewardMapLocation[] commonChest;
        rewardMapLocation[] uncommonKey;
        rewardMapLocation[] uncommonChest;
        rewardMapLocation[] rareKey;
        rewardMapLocation[] rareChest;
        rewardMapLocation[] epicKey;
        rewardMapLocation[] epicChest;
        rewardMapLocation[] legendKey;
        rewardMapLocation[] legendChest;
    }
    // end will removed

    struct rewardMap{
        rewardType mysteryReward;
        rewardRarity mysteryRarity;
        uint256 rewardDeep;
    }

    struct totalRewardRoot{
        uint256 commonKey;
        uint256 commonChest;
        uint256 uncommonKey;
        uint256 uncommonChest;
        uint256 rareKey;
        uint256 rareChest;
        uint256 epicKey;
        uint256 epicChest;
        uint256 legendKey;
        uint256 legendChest;
    }

    struct listRewardRoot{
        uint256[] commonKey;
        uint256[] commonChest;
        uint256[] uncommonKey;
        uint256[] uncommonChest;
        uint256[] rareKey;
        uint256[] rareChest;
        uint256[] epicKey;
        uint256[] epicChest;
        uint256[] legendKey;
        uint256[] legendChest;
    }

    enum rewardType{
        key,
        chest
    }

    enum rewardRarity{
        common,
        uncommon,
        rare,
        epic,
        legend
    }

    function getTotalMapStructure() public view returns (uint256) {
        return totalMapStructure.current();
    }

    function getRewardRules(
        rewardRarity rarity,
        rewardType reward
    ) public view returns (uint256) {
        return rewardRules[rarity][reward];
    }

    function setRewardRules(
        rewardRarity rarity,
        rewardType reward,
        uint256 totalReward
    ) external onlyOwner nonReentrant {
        rewardRules[rarity][reward] = totalReward;
    }

    function generateMapReward() external onlyOwner nonReentrant {
        totalMapStructure.increment();
        
        uint256 currentMap = totalMapStructure.current();
        (totalRewardRoot memory temp,
        listRewardRoot memory roots) = _getRootList();

        for(uint256 a; a < temp.commonKey; a++){
            uint256 x = roots.commonKey[a] % maxX;
            uint256 y = (roots.commonKey[a] - block.number) % maxY;
            uint256 deep = roots.commonKey[a] % maxDeep;
            mapStructure[currentMap][x][y] = rewardMap(
                rewardType.key,
                rewardRarity.common,
                deep
            );
        }
        for(uint256 a; a < temp.commonChest; a++){
            uint256 x = roots.commonChest[a] % maxX;
            uint256 y = (roots.commonChest[a] - block.number) % maxY;
            uint256 deep = roots.commonChest[a] % maxDeep;
            mapStructure[currentMap][x][y] = rewardMap(
                rewardType.chest,
                rewardRarity.common,
                deep
            );
        }
        for(uint256 a; a < temp.uncommonKey; a++){
            uint256 x = roots.uncommonKey[a] % maxX;
            uint256 y = (roots.uncommonKey[a] - block.number) % maxY;
            uint256 deep = roots.uncommonKey[a] % maxDeep;
            mapStructure[currentMap][x][y] = rewardMap(
                rewardType.key,
                rewardRarity.uncommon,
                deep
            );
        }
        for(uint256 a; a < temp.uncommonChest; a++){
            uint256 x = roots.uncommonChest[a] % maxX;
            uint256 y = (roots.uncommonChest[a] - block.number) % maxY;
            uint256 deep = roots.uncommonChest[a] % maxDeep;
            mapStructure[currentMap][x][y] = rewardMap(
                rewardType.chest,
                rewardRarity.uncommon,
                deep
            );
        }
        for(uint256 a; a < temp.rareKey; a++){
            uint256 x = roots.rareKey[a] % maxX;
            uint256 y = (roots.rareKey[a] - block.number) % maxY;
            uint256 deep = roots.rareKey[a] % maxDeep;
            mapStructure[currentMap][x][y] = rewardMap(
                rewardType.key,
                rewardRarity.rare,
                deep
            );
        }
        for(uint256 a; a < temp.rareChest; a++){
            uint256 x = roots.rareChest[a] % maxX;
            uint256 y = (roots.rareChest[a] - block.number) % maxY;
            uint256 deep = roots.rareChest[a] % maxDeep;
            mapStructure[currentMap][x][y] = rewardMap(
                rewardType.chest,
                rewardRarity.rare,
                deep
            );
        }
        for(uint256 a; a < temp.epicKey; a++){
            uint256 x = roots.epicKey[a] % maxX;
            uint256 y = (roots.epicKey[a] - block.number) % maxY;
            uint256 deep = roots.epicKey[a] % maxDeep;
            mapStructure[currentMap][x][y] = rewardMap(
                rewardType.key,
                rewardRarity.epic,
                deep
            );
        }
        for(uint256 a; a < temp.epicChest; a++){
            uint256 x = roots.epicChest[a] % maxX;
            uint256 y = (roots.epicChest[a] - block.number) % maxY;
            uint256 deep = roots.epicChest[a] % maxDeep;
            mapStructure[currentMap][x][y] = rewardMap(
                rewardType.chest,
                rewardRarity.epic,
                deep
            );
        }
        for(uint256 a; a < temp.legendKey; a++){
            uint256 x = roots.legendKey[a] % maxX;
            uint256 y = (roots.legendKey[a] - block.number) % maxY;
            uint256 deep = roots.legendKey[a] % maxDeep;
            mapStructure[currentMap][x][y] = rewardMap(
                rewardType.key,
                rewardRarity.legend,
                deep
            );
        }
        for(uint256 a; a < temp.legendChest; a++){
            uint256 x = roots.legendChest[a] % maxX;
            uint256 y = (roots.legendChest[a] - block.number) % maxY;
            uint256 deep = roots.legendChest[a] % maxDeep;
            mapStructure[currentMap][x][y] = rewardMap(
                rewardType.chest,
                rewardRarity.legend,
                deep
            );
        }
    }

    // for debug, will removed
    function _getCurrentBlock() public view returns (uint256) {
        return block.number;
    }

    function _getMaxRarity() public pure returns (uint256) {
        return 5;
    }

    function _getMaxType() public pure returns (uint256) {
        return 2;
    }

    function _getRewardLocation() public view returns (listRewardLocation memory location) {
        (totalRewardRoot memory temp,
        listRewardRoot memory roots) = _getRootList();

        rewardMapLocation[] memory commonKey = new rewardMapLocation[](temp.commonKey);
        rewardMapLocation[] memory commonChest = new rewardMapLocation[](temp.commonChest);
        rewardMapLocation[] memory uncommonKey = new rewardMapLocation[](temp.uncommonKey);
        rewardMapLocation[] memory uncommonChest = new rewardMapLocation[](temp.uncommonChest);
        rewardMapLocation[] memory rareKey = new rewardMapLocation[](temp.rareKey);
        rewardMapLocation[] memory rareChest = new rewardMapLocation[](temp.rareChest);
        rewardMapLocation[] memory epicKey = new rewardMapLocation[](temp.epicKey);
        rewardMapLocation[] memory epicChest = new rewardMapLocation[](temp.epicChest);
        rewardMapLocation[] memory legendKey = new rewardMapLocation[](temp.legendKey);
        rewardMapLocation[] memory legendChest = new rewardMapLocation[](temp.legendChest);

        for(uint256 a; a < temp.commonKey; a++){
            uint256 x = roots.commonKey[a] % maxX;
            uint256 y = (roots.commonKey[a] - block.number) % maxY;
            uint256 deep = roots.commonKey[a] % maxDeep;
            commonKey[a] = rewardMapLocation(
                rewardType.key,
                rewardRarity.common,
                deep,
                x,
                y
            );
        }
        for(uint256 a; a < temp.commonChest; a++){
            uint256 x = roots.commonChest[a] % maxX;
            uint256 y = (roots.commonChest[a] - block.number) % maxY;
            uint256 deep = roots.commonChest[a] % maxDeep;
            commonChest[a] = rewardMapLocation(
                rewardType.chest,
                rewardRarity.common,
                deep,
                x,
                y
            );
        }
        for(uint256 a; a < temp.uncommonKey; a++){
            uint256 x = roots.uncommonKey[a] % maxX;
            uint256 y = (roots.uncommonKey[a] - block.number) % maxY;
            uint256 deep = roots.uncommonKey[a] % maxDeep;
            uncommonKey[a] = rewardMapLocation(
                rewardType.key,
                rewardRarity.uncommon,
                deep,
                x,
                y
            );
        }
        for(uint256 a; a < temp.uncommonChest; a++){
            uint256 x = roots.uncommonChest[a] % maxX;
            uint256 y = (roots.uncommonChest[a] - block.number) % maxY;
            uint256 deep = roots.uncommonChest[a] % maxDeep;
            uncommonChest[a] = rewardMapLocation(
                rewardType.chest,
                rewardRarity.uncommon,
                deep,
                x,
                y
            );
        }
        for(uint256 a; a < temp.rareKey; a++){
            uint256 x = roots.rareKey[a] % maxX;
            uint256 y = (roots.rareKey[a] - block.number) % maxY;
            uint256 deep = roots.rareKey[a] % maxDeep;
            rareKey[a] = rewardMapLocation(
                rewardType.key,
                rewardRarity.rare,
                deep,
                x,
                y
            );
        }
        for(uint256 a; a < temp.rareChest; a++){
            uint256 x = roots.rareChest[a] % maxX;
            uint256 y = (roots.rareChest[a] - block.number) % maxY;
            uint256 deep = roots.rareChest[a] % maxDeep;
            rareChest[a] = rewardMapLocation(
                rewardType.chest,
                rewardRarity.rare,
                deep,
                x,
                y
            );
        }
        for(uint256 a; a < temp.epicKey; a++){
            uint256 x = roots.epicKey[a] % maxX;
            uint256 y = (roots.epicKey[a] - block.number) % maxY;
            uint256 deep = roots.epicKey[a] % maxDeep;
            epicKey[a] = rewardMapLocation(
                rewardType.key,
                rewardRarity.epic,
                deep,
                x,
                y
            );
        }
        for(uint256 a; a < temp.epicChest; a++){
            uint256 x = roots.epicChest[a] % maxX;
            uint256 y = (roots.epicChest[a] - block.number) % maxY;
            uint256 deep = roots.epicChest[a] % maxDeep;
            epicChest[a] = rewardMapLocation(
                rewardType.chest,
                rewardRarity.epic,
                deep,
                x,
                y
            );
        }
        for(uint256 a; a < temp.legendKey; a++){
            uint256 x = roots.legendKey[a] % maxX;
            uint256 y = (roots.legendKey[a] - block.number) % maxY;
            uint256 deep = roots.legendKey[a] % maxDeep;
            legendKey[a] = rewardMapLocation(
                rewardType.key,
                rewardRarity.legend,
                deep,
                x,
                y
            );
        }
        for(uint256 a; a < temp.legendChest; a++){
            uint256 x = roots.legendChest[a] % maxX;
            uint256 y = (roots.legendChest[a] - block.number) % maxY;
            uint256 deep = roots.legendChest[a] % maxDeep;
            legendChest[a] = rewardMapLocation(
                rewardType.chest,
                rewardRarity.legend,
                deep,
                x,
                y
            );
        }

        location.commonKey = commonKey;
        location.commonChest = commonChest;
        location.uncommonKey = uncommonKey;
        location.uncommonChest = uncommonChest;
        location.rareKey = rareKey;
        location.rareChest = rareChest;
        location.epicKey = epicKey;
        location.epicChest = epicChest;
        location.legendKey = legendKey;
        location.legendChest = legendChest;
    }
    // end of removed 

    function _getRootList() public view returns (
        totalRewardRoot memory temp,
        listRewardRoot memory roots
    ) {
        uint256 nextCount;
        uint256 currentId = totalMapStructure.current();

        // totalRewardRoot memory temp = _getTotalHash();
        temp = _getTotalHash();

        uint256[] memory commonKey = new uint256[](temp.commonKey);
        uint256[] memory commonChest = new uint256[](temp.commonChest);
        uint256[] memory uncommonKey = new uint256[](temp.uncommonKey);
        uint256[] memory uncommonChest = new uint256[](temp.uncommonChest);
        uint256[] memory rareKey = new uint256[](temp.rareKey);
        uint256[] memory rareChest = new uint256[](temp.rareChest);
        uint256[] memory epicKey = new uint256[](temp.epicKey);
        uint256[] memory epicChest = new uint256[](temp.epicChest);
        uint256[] memory legendKey = new uint256[](temp.legendKey);
        uint256[] memory legendChest = new uint256[](temp.legendChest);

        for(uint256 a; a < temp.commonKey; a++){
            commonKey[a] =  _getHashValue(
                currentId,
                nextCount,
                block.number
            );
            nextCount += 1;
        }
        for(uint256 a; a < temp.commonChest; a++){
            commonChest[a] =  _getHashValue(
                currentId,
                nextCount,
                block.number
            );
            nextCount += 1;
        }
        for(uint256 a; a < temp.uncommonKey; a++){
            uncommonKey[a] =  _getHashValue(
                currentId,
                nextCount,
                block.number
            );
            nextCount += 1;
        }
        for(uint256 a; a < temp.uncommonChest; a++){
            uncommonChest[a] =  _getHashValue(
                currentId,
                nextCount,
                block.number
            );
            nextCount += 1;
        }
        for(uint256 a; a < temp.rareKey; a++){
            rareKey[a] =  _getHashValue(
                currentId,
                nextCount,
                block.number
            );
            nextCount += 1;
        }
        for(uint256 a; a < temp.rareChest; a++){
            rareChest[a] =  _getHashValue(
                currentId,
                nextCount,
                block.number
            );
            nextCount += 1;
        }
        for(uint256 a; a < temp.epicKey; a++){
            epicKey[a] =  _getHashValue(
                currentId,
                nextCount,
                block.number
            );
            nextCount += 1;
        }
        for(uint256 a; a < temp.epicChest; a++){
            epicChest[a] =  _getHashValue(
                currentId,
                nextCount,
                block.number
            );
            nextCount += 1;
        }
        for(uint256 a; a < temp.legendKey; a++){
            legendKey[a] =  _getHashValue(
                currentId,
                nextCount,
                block.number
            );
            nextCount += 1;
        }
        for(uint256 a; a < temp.legendChest; a++){
            legendChest[a] =  _getHashValue(
                currentId,
                nextCount,
                block.number
            );
            nextCount += 1;
        }

        roots.commonKey = commonKey;
        roots.commonChest = commonChest;
        roots.uncommonKey = uncommonKey;
        roots.uncommonChest = uncommonChest;
        roots.rareKey = rareKey;
        roots.rareChest = rareChest;
        roots.epicKey = epicKey;
        roots.epicChest = epicChest;
        roots.legendKey = legendKey;
        roots.legendChest = legendChest;
    }

    function _getTotalHash() public view returns (totalRewardRoot memory total) {
        total.commonKey = getRewardRules(
            rewardRarity.common,
            rewardType.key
        );
        total.commonChest = getRewardRules(
            rewardRarity.common,
            rewardType.chest
        );
        total.uncommonKey = getRewardRules(
            rewardRarity.uncommon,
            rewardType.key
        );
        total.uncommonChest = getRewardRules(
            rewardRarity.uncommon,
            rewardType.chest
        );
        total.rareKey = getRewardRules(
            rewardRarity.rare,
            rewardType.key
        );
        total.rareChest = getRewardRules(
            rewardRarity.rare,
            rewardType.chest
        );
        total.epicKey = getRewardRules(
            rewardRarity.epic,
            rewardType.key
        );
        total.epicChest = getRewardRules(
            rewardRarity.epic,
            rewardType.chest
        );
        total.legendKey = getRewardRules(
            rewardRarity.legend,
            rewardType.key
        );
        total.legendChest = getRewardRules(
            rewardRarity.legend,
            rewardType.chest
        );
    }

    function _getHashValue(
        uint256 inputId,
        uint256 inputNumber,
        uint256 blockNumber
    ) public view returns (uint256 generatedHash) {
        bytes32 hash = blockhash(blockNumber - 1);
        generatedHash = uint256(
            keccak256(
                abi.encodePacked(
                    block.chainid,
                    inputId,
                    hash,
                    inputNumber
                )
            )
        );
    }
}
